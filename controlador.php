<?php
require_once 'data.php';

$chat = new Chat();
$acc  = @$_REQUEST['acc']; //Variable que traerá la acción a realizar

if ($acc == 'listar') {
    $ultimoID = isset($_REQUEST['ultimoID']) ? $_REQUEST['ultimoID'] : 0; //Verificamos el último ID
    $lista    = $chat->getMensajes($ultimoID); //Obtenemos los mensajes desde la BD
    echo json_encode(array('lista' => $lista)); //Imprimimos el arreglo en formato JSON

} elseif ($acc == 'enviar') {
    $nick = $_REQUEST['nick'];  //Nick del usuario
    $msj  = $_REQUEST['msj']; //Mensaje
    $id   = $chat->insertarMensaje($nick, $msj); //Último ID insertado
    $resp = "";
    if ($id > 0) {
        $resp = "Se registró :)";
    } else {
        $resp = "No se insertó :(";
    }
    echo json_encode(array('resp' => $resp, 'id' => $id)); //Imprimimos el arreglo en formato JSON
}
