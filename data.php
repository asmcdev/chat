<?php
class Chat
{
    public function __construct()
    {
    	//Conexión a la BD
        $con = mysql_connect('localhost', 'root', '123456');
        mysql_select_db('chat', $con);
    }

    public function getMensajes($ultimoID = 0)
    {
        $q  = mysql_query("SELECT idmensaje, nick, msj, fecha, DATE_FORMAT(fecha, '%H:%i:%s') as hora FROM mensaje WHERE idmensaje > " . mysql_real_escape_string($ultimoID)); //Query para obtener los mensajes
        $ls = array();
        while ($rs = mysql_fetch_object($q)) {
            $ls[] = $rs; //Pasamos cada registro a una posición del arreglo, "$arreglo[]=..." es incremental
        }
        return $ls; //Retornamos la lista de mensajes
    }

    public function insertarMensaje($nick, $mensaje)
    {
        $mensajeID = 0;
        //Query para insertar a la BD el mensaje
        $q         = mysql_query("INSERT INTO mensaje(nick, msj, fecha) VALUES('" . mysql_real_escape_string($nick) . "','" . mysql_real_escape_string($mensaje) . "', NOW())");
        if ($q !== false) {
            $mensajeID = mysql_insert_id(); //Retornamos el último ID insertado
        }
        return $mensajeID;
    }
}
