var chatSomosSistemas = {
	timer:null, //Variable para el temporizador
	ultimoID: 0, //Ultimo ID obtenido desde la BD
	init: function(){ //Función inicial que instancia la llamada al controlador y acciones
		timer = setInterval(function(){ //Instanciamos el temporizador para que ejecute una función por 1 seg (x1000)
			chatSomosSistemas.recargar();
		}, 1000);

		$('#frmChat').submit(function(ev){ //Acción que ejecutará el controlador para enviar el mensaje
			ev.preventDefault();
			chatSomosSistemas.enviar();
		});
	},
	recargar: function(){ //Función que traerá la lista de mensajes desde el último ID
		$.post('controlador.php?acc=listar&ultimoID='+chatSomosSistemas.ultimoID, function(resp, status){
			$.each(resp.lista, function(idx, obj){
				chatSomosSistemas.ultimoID = obj.idmensaje; //Mantenemos siempre actualizado el último ID
				
				//Estructura del mensaje
				$('#chat').append('<div class="mensaje" id="mensaje_'+obj.idmensaje+'">'
										+'<span class="nick">'+obj.nick+'</span> <span class="hora">('+obj.hora+')</span><br>'
										+'<span class="msj">'+obj.msj+'</span>'
									+'</div>');
			});
			if(resp.lista.length > 0){
				$('#chat').scrollTop($('#chat')[0].scrollHeight); //Enviará o desplazará el scroll hacia el final
			}
		}, 'json');
	},
	enviar: function(){
		$.post('controlador.php?acc=enviar', $('#frmChat').serialize(), function(resp, status){
			if(resp.id == 0){
				alert(resp.resp); //Mostrará alerta en caso de que no se haya insertado
			}else{
				$('#txtMensaje').val(''); //Limpiará el input del mensaje
				$('#txtNick').prop('readonly', true); //El nick no podrá ser modificado
				$('#txtMensaje').focus(); //Se resalta el cursor en el input del mensaje
			}
		}, 'json');
	}
}

$(function(){
	chatSomosSistemas.init(); //Inicializamos el chat
});